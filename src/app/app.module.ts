import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DashboardTwoComponent } from './components/dashboard-two/dashboard-two.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    DashboardTwoComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
